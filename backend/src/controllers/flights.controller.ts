import { JsonController, Get, Put, BodyParam } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService();

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Put('')
    async updateFlight(@BodyParam('status') status: String, @BodyParam('code') code: String) {
        await flightsService.updateFlightStatus(code, status)
        return {
            status: 200
        }
    }
}
