import { FlightsModel } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async updateFlightStatus(code: String, status: String) {
        const filter = { code }
        const update = { status }
        return await FlightsModel.findOneAndUpdate(filter, update)
    }
}
